#!/usr/bin/python3

import re
import os
from astropy.io import fits


def listDir(directory, pattern):
    # prevedu bash-like regexp na python-like
    pattern = "^" + pattern.replace(".", "\\.").replace("*", ".*").replace("?", ".") + "$"
    # ve Windowsich se ignoruje velikost pismen
    regexp = re.compile(pattern, re.IGNORECASE)
    # projedu si vsechny soubory v adresari (prevodem na pole se zbavim generatoru)
    return list(filter(lambda x: regexp.match(x), os.listdir(directory)))


def readTemp(directory, filenames):
    # druhy parametr je pole se jmeny souboru
    for filename in filenames:
        # abych soubor nacetl, musim ho spojit s cestou k nactenemu adresari
        with fits.open(directory + os.sep + filename) as hdul:
            # vybral jsem jeden parametr
            yield hdul[0].header['CCD-TEMP']


def crawlDir(directory, pattern):
    # nejdriv si projdu vsechny soubory, ktere vyhovuji masce
    names = listDir(directory, pattern)
    # zip() slouci dve pole do jednoho pole dvojic, ze kterych jde snadno udelat slovnik
    temps = dict(zip(names, readTemp(directory, names)))
    return temps


def printResults(temps):
    # radeji se nejdrive vyhnu prazdnemu vyctu, abych nedelil 0
    if len(temps) == 0:
        return print('nenalezen zadny soubor..')
    # zjistim nejvyssi delku souboru, aby se spravne zarovnal vystup
    alignedLen = max([len(x) for x in temps]) + 1
    alignedLen = max(7, alignedLen)

    for name, temp in temps.items():
        alignedName = name + ' ' * (alignedLen - len(name))
        print(alignedName, temp)
    print('-' * (alignedLen + 5))
    # nakonec zprumeruju vsechny nactene teploty
    avgTemp = sum(temps.values()) / len(temps)
    print('prumer' + ' ' * (alignedLen - 6), "%1.1f" % avgTemp)


if __name__ == '__main__':
    """ Priklad pouziti na testovacich datech """

    from sys import argv
    if len(argv) < 2:
        print('potrebuju pattern (treba *.fit) jako argument!')

    dir = 'data/2021_08_06_Kolonice_Amerika_M27' if len(argv) < 3 else argv[2]
    maska = argv[1]

    out = crawlDir(dir, maska)
    printResults(out)
